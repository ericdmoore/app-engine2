#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Intern
#
# Created:     11/07/2013
# Copyright:   (c) Intern 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import logging
import jinja2
import os
import webapp2
import logging
import cgi
import smtplib
import tempfile
import base64
import time
import StringIO
import SimpleHTTPServer
import SocketServer
import datetime
from decimal import *
from ..lib import xlwt
from ..lib.xlsxwriter.workbook import Workbook
from ..lib.xlsxwriter.worksheet import Worksheet
from ..lib import MySQLdb
from ..models.greeting import Greeting as Geeting
from ..lib import Offer_Metrics_Worksheet
from ..lib import Users_Worksheet
from ..lib import Stores_Worksheet
from ..models.ReportList import ReportList as List
from ..models.TabList import TabsStatic as Tstat,TabsDynamic as Tdyn

from google.appengine.ext import ndb
from google.appengine.api import users
from google.appengine.api import mail
from google.appengine.api.mail import EmailMessage
from google.appengine.api import taskqueue

jinja2_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'])


class Report(webapp2.RequestHandler):
    def get(self):
        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template = jinja2_environment.get_template('Sample_Email.html')
        self.response.out.write(template.render(
                                                url=url,
                                                url_linktext=url_linktext))
    def post(self):
        getcontext().prec = 3
        report = xlwt.Workbook()
        pattern = xlwt.Pattern()
        pattern.pattern = xlwt.Pattern.SOLID_PATTERN
        pattern.pattern_fore_colour = 0x3A
        style = xlwt.easyxf('font: height 200, name Tahoma, color 0x09, bold on; align: wrap on, vert center, horiz right;')
        style1 = xlwt.easyxf('font: height 200, name Tahoma, color 0x09, bold on; align: wrap on, vert center, horiz left; ')
        style2 = xlwt.easyxf('font: height 280, name Tahoma, color 0x08; align: wrap on, vert center, horiz right')
        style3 = xlwt.easyxf('font: height 280, name Tahoma, color 0x08; align: wrap on, vert center, horiz left')
        style4 = xlwt.easyxf('font: height 200, name Tahoma, color 0x08, bold on; align: wrap on, vert center, horiz left')
        style5 = xlwt.easyxf('font: height 200, name Tahoma, color 0x08, bold on; align: wrap on, vert center, horiz left')
        style6 = xlwt.easyxf('font: height 240, name Tahoma, color 0x08, bold on; align: wrap on, vert center, horiz right')
        style7 = xlwt.easyxf('font: height 240, name Tahoma, color 0x08; align: wrap on, vert center, horiz right')


        style.pattern = pattern
        style1.pattern = pattern

        reportname = self.request.get('ReportName')
        clientid = self.request.get('ID')
        clientName = self.request.get('Name')
        datestart = datetime.datetime.strptime(self.request.get('Start'),"%Y-%m-%dT%H:%M")
        dateend = datetime.datetime.strptime(self.request.get('End'),"%Y-%m-%dT%H:%M")
        date_start = self.request.get('Start')
        date_end = self.request.get('End')

        logging.info(clientid)

        '''reporter = List()
        reporter.clientName = clientName
        reporter.ClientId = str(clientid)
        reporter.reportName = reportname
        reporter.date_start = datestart
        reporter.date_end = dateend
        if self.request.get('User'):
            reporter.user = True
        if self.request.get('Offer'):
            reporter.offer = True
        if self.request.get('Store'):
            reporter.store = True
        if self.request.get('Michaels'):
            reporter.michaels = True
        if self.request.get('Week'):
            reporter.frequency = "Weekly"
            reporter.weekly = True
            reporter.Scheduled = self.request.get('weekday')
        if self.request.get('Month'):
            reporter.frequency = "Monthly"
            reporter.monthly = True
            reporter.Scheduled = self.request.get('monthday')
        reporter.date_created = datetime.datetime.now()
        reporter.put()
        '''

        # Step 1. connecting to database
        db = MySQLdb.connect(host="reports.kouponmedia.com",
                     port=3306,
                     user="koupon-read",
                      passwd="koupon01!!",
                      db="km_analytics")

        # Step 2. declaring cursor variable
        cur = db.cursor()



        # Step 3.1 Calling function Users for generating Users worksheet
        if self.request.get('User'):
            user_sheet = report.add_sheet('User Metrics')
            user_sheet.col(2).width = 256 * 35
            for i in range(3,15):
                user_sheet.col(i).width = 256 *25
            user_sheet.show_grid = False

            user_sheet.write(2,2,'Client Name',style2)
            user_sheet.write(3,2,'Client ID',style2)
            user_sheet.write(4,2,'Starting',style2)
            user_sheet.write(5,2,'Duration',style2)
            user_sheet.write(6,2,'Include',style2)

            user_sheet.write(2,4,clientName,style3)
            user_sheet.write(3,4,clientid,style3)
            user_sheet.write(4,4,date_start,style3)
            user_sheet.write(5,4,'1 Week',style3)
            user_sheet.write(6,4,'3 Weeks',style3)

            user_sheet.write(9,2,'User Metrics', style1)
            user_sheet.write(10,2,'Total User Base Beginning')
            user_sheet.write(11,2,'Total New Users')
            user_sheet.write(12,2,'Total User Base End',style4)
            user_sheet.write(13,2,' User Base Growth Rate')
            user_sheet.write(14,2,'Active Users',style4)
            user_sheet.write(15,2,'Increase(Decrease) Previous Period')
            user_sheet.write(9,3,'Current Week',style)
            user_sheet.write(9,4,'Previous',style)
            user_sheet.write(9,5,'2 Weeks Prior',style)
            user_sheet.write(9,6,'3 Weeks Prior',style)
            user_sheet.write(17,2,'Aggregate Offer Metrics',style1)
            user_sheet.write(19,2,'Views:')
            user_sheet.write(20,2,'Offers Presented During Period')
            user_sheet.write(21,2,'Total Offer Views',style4)
            user_sheet.write(22,2,'Average # Offer Views Per User')
            user_sheet.write(24,2,'Redemptions')
            user_sheet.write(25,2,'New User Redemptions')
            user_sheet.write(26,2,'Existing User Redemptions')
            user_sheet.write(27,2,'Total Redemptions',style4)
            user_sheet.write(28,2,'Redemption Ratio')
            user_sheet.write(29,2,'Increase(Decrease) Previous Period')
            user_sheet.write(30,2,'Average # Offer Views Per Active User')
            user_sheet.write(32,2,'Unique User Redemptions:')
            user_sheet.write(33,2,'Four Weeks Consecutive')
            Cons_4 = Users_Worksheet.query_cons_usr_redeem_4(cur, clientid, datestart, dateend)
            num = 0
            for row in Cons_4:
                for item in row:
                    if(num == 0):
                        user_sheet.write(33,3,str(item),style7)

            user_sheet.write(34,2,'Three Out of Four Weeks')
            Cons_3 = Users_Worksheet.query_cons_usr_redeem_3(cur, clientid, datestart, dateend)
            num = 0
            for row in Cons_3:
                for item in row:
                    if(num == 0):
                        user_sheet.write(34,3,str(item),style7)

            user_sheet.write(35,2,'Two Out of Four Weeks')
            Cons_2 = Users_Worksheet.query_cons_usr_redeem_2(cur, clientid, datestart, dateend)
            num = 0
            for row in Cons_2:
                for item in row:
                    if(num == 0):
                        user_sheet.write(35,3,str(item),style7)

            user_sheet.write(36,2,'One Out of Four Weeks')
            Cons_1 = Users_Worksheet.query_cons_usr_redeem_1(cur, clientid, datestart, dateend)
            num = 0
            for row in Cons_1:
                for item in row:
                    if(num == 0):
                        user_sheet.write(36,3,str(item),style7)


            user_sheet.write(17,3,'Current Week', style)
            Agg_week = Users_Worksheet.query_wk4(cur, clientid, datestart, dateend)
            Act_users = Users_Worksheet.query_act_users_4(cur,clientid,datestart,dateend)

            user_sheet.write(20,3,str(Agg_week[0]),style7)
            user_sheet.write(21,3,str(Agg_week[1]),style7)
            user_sheet.write(14,3,str(Act_users),style7)
            user_sheet.write(27,3,str(Agg_week[3]),style7)

            Exist_user = Users_Worksheet.query_per_week_4(cur, clientid, datestart, dateend)
            user_sheet.write(12,3,str(Exist_user),style7)

            Exist_red = Users_Worksheet.query_usr_redeem_1(cur, clientid, datestart, dateend)
            user_sheet.write(26,3,str(Exist_red),style7)

            user_sheet.write(17,4,'Previous',style)
            Agg_week = Users_Worksheet.query_wk3(cur, clientid, datestart, dateend)
            Act_users = Users_Worksheet.query_act_users_3(cur,clientid,datestart,dateend)

            user_sheet.write(20,4,str(Agg_week[0]),style7)
            user_sheet.write(21,4,str(Agg_week[1]),style7)
            user_sheet.write(14,4,str(Act_users),style7)
            user_sheet.write(27,4,str(Agg_week[3]),style7)

            Exist_user = Users_Worksheet.query_per_week_3(cur, clientid, datestart, dateend)
            user_sheet.write(12,4,str(Exist_user),style7)

            Exist_red = Users_Worksheet.query_usr_redeem_2(cur, clientid, datestart, dateend)
            user_sheet.write(26,4,str(Exist_red),style7)


            user_sheet.write(17,5,'2 Weeks Prior',style)
            Agg_week = Users_Worksheet.query_wk2(cur, clientid, datestart, dateend)
            Act_users = Users_Worksheet.query_act_users_2(cur,clientid,datestart,dateend)

            user_sheet.write(20,5,str(Agg_week[0]),style7)
            user_sheet.write(21,5,str(Agg_week[1]),style7)
            user_sheet.write(14,5,str(Act_users),style7)
            user_sheet.write(27,5,str(Agg_week[3]),style7)

            Exist_user = Users_Worksheet.query_per_week_2(cur, clientid, datestart, dateend)
            user_sheet.write(12,5,str(Exist_user),style7)

            Exist_red = Users_Worksheet.query_usr_redeem_3(cur, clientid, datestart, dateend)
            user_sheet.write(26,5,str(Exist_red),style7)

            user_sheet.write(17,6,'3 Weeks Prior',style)

            Agg_week = Users_Worksheet.query_wk1(cur, clientid, datestart, dateend)
            Act_users = Users_Worksheet.query_act_users_1(cur,clientid,datestart,dateend)

            user_sheet.write(20,6,str(Agg_week[0]),style7)
            user_sheet.write(21,6,str(Agg_week[1]),style7)
            user_sheet.write(14,6,str(Act_users),style7)
            user_sheet.write(27,6,str(Agg_week[3]),style7)

            Exist_user = Users_Worksheet.query_per_week_1(cur, clientid, datestart, dateend)
            user_sheet.write(12,6,str(Exist_user),style7)

            Exist_red = Users_Worksheet.query_usr_redeem_3(cur, clientid, datestart, dateend)
            user_sheet.write(26,6,str(Exist_red),style7)

            user_sheet.write(10,3,xlwt.Formula('E13'), style7)
            user_sheet.write(10,4,xlwt.Formula('F13'), style7)
            user_sheet.write(10,5,xlwt.Formula('G13'), style7)

            user_sheet.write(11,3,xlwt.Formula('D13 - D11'), style7)
            user_sheet.write(11,4,xlwt.Formula('E13 - E11'), style7)
            user_sheet.write(11,5,xlwt.Formula('F13 - F11'), style7)
            user_sheet.write(11,6,xlwt.Formula('G13 - G11'), style7)

            user_sheet.write(25,3,xlwt.Formula('D28 - D27'), style7)
            user_sheet.write(25,4,xlwt.Formula('E28 - E27'), style7)
            user_sheet.write(25,5,xlwt.Formula('F28 - F27'), style7)
            user_sheet.write(25,6,xlwt.Formula('G28 - G27'), style7)

            user_sheet.write(22,3,xlwt.Formula('D22/D15'), style7)
            user_sheet.write(22,4,xlwt.Formula('E22/E15'), style7)
            user_sheet.write(22,5,xlwt.Formula('F22/F15'), style7)
            user_sheet.write(22,6,xlwt.Formula('G22/G15'), style7)

            user_sheet.write(13,3,xlwt.Formula('D12/D11 * 100'), style7)
            user_sheet.write(13,4,xlwt.Formula('E12/E11 * 100'), style7)
            user_sheet.write(13,5,xlwt.Formula('F12/F11 * 100'), style7)
            user_sheet.write(13,6,xlwt.Formula('G12/G11 * 100'), style7)

            user_sheet.write(15,3,xlwt.Formula('(D15-E15)/E15 * 100'), style7)
            user_sheet.write(15,4,xlwt.Formula('(E15-F15)/F15 * 100'), style7)
            user_sheet.write(15,5,xlwt.Formula('(F15-G15)/G15 * 100'), style7)

            user_sheet.write(28,3,xlwt.Formula('D28/D22 * 100'), style7)
            user_sheet.write(28,4,xlwt.Formula('E28/E22 * 100'), style7)
            user_sheet.write(28,5,xlwt.Formula('F28/F22 * 100'), style7)
            user_sheet.write(28,6,xlwt.Formula('G28/G22 * 100'), style7)

            user_sheet.write(29,3,xlwt.Formula('(D28-E28)/E28 * 100'), style7)
            user_sheet.write(29,4,xlwt.Formula('(E28-F28)/F28 * 100'), style7)
            user_sheet.write(29,5,xlwt.Formula('(F28-G28)/G28 * 100'), style7)

            user_sheet.write(30,3,xlwt.Formula('D28/D15'), style7)
            user_sheet.write(30,4,xlwt.Formula('E28/E15'), style7)
            user_sheet.write(30,5,xlwt.Formula('F28/F15'), style7)
            user_sheet.write(30,6,xlwt.Formula('G28/G15'), style7)

        if self.request.get('Offer'):
            offer_sheet = report.add_sheet('Offer Metrics')

            offer_sheet.col(2).width = 256 * 35
            for i in range(3,15):
                offer_sheet.col(i).width = 256 *25
            offer_sheet.show_grid = False

            offer_sheet.write(2,2,'Client Name',style2)
            offer_sheet.write(3,2,'Client ID',style2)
            offer_sheet.write(4,2,'Starting',style2)
            offer_sheet.write(5,2,'Duration',style2)
            offer_sheet.write(6,2,'Include',style2)

            offer_sheet.write(2,4,clientName,style3)
            offer_sheet.write(3,4,clientid,style3)
            offer_sheet.write(4,4,date_start,style3)
            offer_sheet.write(5,4,'1 Week',style3)
            offer_sheet.write(6,4,'3 Weeks',style3)

            offer_sheet.write(9,2,'Benchmark Metrics',style1)
            offer_sheet.write(9,3,'Unique Views',style)
            offer_sheet.write(9,4,'Barcode', style)
            offer_sheet.write(9,5,'Redemption Ratio',style)
            offer_sheet.write(9,6,'Avg Views/User',style)
            offer_sheet.write(9,7,'TimeTaken',style)
            offer_sheet.write(10,2,'Average Offer/Day',style4)

            benchweek = Offer_Metrics_Worksheet.benchmark_week(cur, clientid, datestart, dateend)
            offer_sheet.write(10, 3,str(benchweek[0]),style7)
            offer_sheet.write(10, 4,str(benchweek[1]),style7)
            offer_sheet.write(10, 5,str(benchweek[2]),style7)
            offer_sheet.write(10, 6,str(benchweek[3]),style7)


            offer_sheet.write(11,2,'Average Offer YTD',style4)
            benchweek = Offer_Metrics_Worksheet.benchmark_ytd(cur, clientid, datestart, dateend)
            offer_sheet.write(11, 3,str(benchweek[0]),style7)
            offer_sheet.write(11, 4,str(benchweek[1]),style7)
            offer_sheet.write(11, 5,str(benchweek[2]),style7)
            offer_sheet.write(11, 6,str(benchweek[3]),style7)

            offer_sheet.write(12,2,'Average Offer All Time',style4)
            benchweek = Offer_Metrics_Worksheet.benchmark_all(cur, clientid, datestart, dateend)
            offer_sheet.write(12, 3,str(benchweek[0]),style7)
            offer_sheet.write(12, 4,str(benchweek[1]),style7)
            offer_sheet.write(12, 5,str(benchweek[2]),style7)
            offer_sheet.write(12, 6,str(benchweek[3]),style7)

            offer_sheet.write(14,2,'Offer Metrics',style1)
            offer_sheet.write(14,3,'Unique Views',style)
            offer_sheet.write(14,4,'Barcode',style)
            offer_sheet.write(14,5,'Redemption Ratio',style)
            offer_sheet.write(14,6,'Avg Views/User',style)
            offer_sheet.write(14,7,'TimeTaken',style)

            offer = Offer_Metrics_Worksheet.Offer(cur,clientid,datestart,dateend)
            rown = 15
            for row in offer:
                coln = 2
                for item in row:
                    if item!=row[1]:
                        offer_sheet.write(rown,coln,str(item),style7)
                        coln = coln+1
                rown = rown+1

        if self.request.get('Store'):
            store_sheet = report.add_sheet('Store Metrics')

            store_sheet.col(2).width = 256 * 35
            for i in range(3,15):
                store_sheet.col(i).width = 256 *25
            store_sheet.show_grid = False

            store_sheet.write(2,2,'Client Name',style2)
            store_sheet.write(3,2,'Client ID',style2)
            store_sheet.write(4,2,'Starting',style2)
            store_sheet.write(5,2,'Duration',style2)
            store_sheet.write(6,2,'Include',style2)

            store_sheet.write(2,4,clientName,style3)
            store_sheet.write(3,4,clientid,style3)
            store_sheet.write(4,4,date_start,style3)
            store_sheet.write(5,4,'1 Week',style3)
            store_sheet.write(6,4,'3 Weeks',style3)

            store_sheet.write(9,2,'Store Metrics',style1)
            store_sheet.write(9,3,'Store',style1)
            store_sheet.write(9,4,'Redeemed',style)

            store_wk = Stores_Worksheet.Store_Week(cur,clientid,datestart,dateend)
            rown = 10
            for row in store_wk:
                coln = 3
                for item in row:
                    store_sheet.write(rown,coln,str(item),style7)
                    coln = coln+1
                rown = rown+1

            store_sheet.write(9,6,'Aggregate Store Metrics',style)
            store_sheet.write(9,7,'Store ',style)
            store_sheet.write(9,8,'Aggregate Redemption ',style)
            store_ag = Stores_Worksheet.Store_Agg(cur,clientid,datestart,dateend)
            rown = 10
            for row in store_ag:
                coln = 7
                for item in row:
                    store_sheet.write(rown,coln,str(item),style7)
                    coln = coln+1
                rown = rown+1
        self.response.headers['Content-Type'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        self.response.headers['Content-Transfer-Encoding'] = 'Binary'
        self.response.headers['Content-disposition'] = 'attachment; filename="Test1.xls"'
        report.save(self.response.out)
        subject = '7-Eleven Report'
        body = 'I chose not to send your whole email back - just wanted to acknowledge that you sent one.'
        cc = ''
        attachment = 'Test1.xls'
        logging.info(attachment)
        taskqueue.add(url='/worker/emailer',
    			  params={'sender': 'test@koupon.appspot.com', #echo back the variable inbox
    			  		  'to': 'rjain@kou.pn',  #send back to the original sender
    			  		  'cc':cc,                   #reply w/ cc if present
                          'subject': subject, 		 #pre-canned subject
                          'body':body,
                          'attachment':attachment})

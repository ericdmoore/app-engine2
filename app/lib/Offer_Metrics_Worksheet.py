#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Nitesh
#
# Created:     19/06/2013
# Copyright:   (c) Nitesh 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import MySQLdb
import sys
import datetime
from datetime import date, timedelta
import time
from decimal import *
getcontext().prec = 3
def Offer(cur,client_id,start,end):
        # File: Offer
        # To Extract the Offer Query Data

    start_dt = end - timedelta(days=7)

    select_record ="""SELECT
                         t1.promo_id,
                         count((t1.consumer_identity)) AS Views,
                         count(distinct(t1.consumer_identity)) As UniqueViews,
                         sum(case when t1.consumer_offer_transaction_state in ("Presented") then 1 else null end) AS Barcode,
                         sum(case when t1.consumer_offer_transaction_state in ("Presented") then 1 else null end)/count(distinct(t1.consumer_identity)) * 100,
                         count((t1.consumer_identity))/count(distinct(t1.consumer_identity))
                         FROM
							km_analytics.consumer_offer_transaction t1
                          where t1.client_id = %s
                          and t1.consumer_offer_transaction_date between '%s' and '%s'
                        group by t1.promo_id """ % (client_id,start_dt,end)
        # execute the SQL query using execute() method.
    cur.execute(select_record)


        # fetch all of the rows from the query
    query_offer_data = cur.fetchall ()
    return query_offer_data


#-------------------------------------------------------------------------------
def benchmark_all(cur,client_id,start,end):
        # File: BenchmarkFinal
        # Query: To extract the Benchmark Metrics Data
        # All Time
        # Year To Date
    select_record ="""SELECT
                         count((t1.consumer_identity)) AS Views,
                         count(distinct(t1.consumer_identity)) As UniqueViews,
                         sum(case when t1.consumer_offer_transaction_state in ("Presented") then 1 else null end) AS Barcode
                         FROM
							km_analytics.consumer_offer_transaction t1
                          where t1.client_id = %s
                        group by t1.promo_id """ % (client_id)

        # execute the SQL query using execute() method.
    cur.execute(select_record)
    # fetch all of the rows from the query
    rows = cur.fetchall ()

    Views = 0
    UniqueViews = 0
    Barcode = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        Views = Views + row[0]
        UniqueViews = UniqueViews + row[1]
        Barcode = Barcode + row[2]

        RedemptionRatio = "{0:.2f}%".format(Barcode/UniqueViews *100)
        AvgUser = "%0.2f" % (Views/UniqueViews)
    return (UniqueViews,Barcode,RedemptionRatio,AvgUser)

#----------------
def benchmark_ytd(cur,client_id,start,end):
        # Year To Date
    select_record ="""SELECT
                         count((t1.consumer_identity)) AS Views,
                         count(distinct(t1.consumer_identity)) As UniqueViews,
                         sum(case when t1.consumer_offer_transaction_state in ("Presented") then 1 else null end) AS Barcode
                         FROM
							km_analytics.consumer_offer_transaction t1
                          where t1.client_id = %s
                          and t1.consumer_offer_transaction_date between '%s' and '%s'
                        group by t1.promo_id """ % (client_id,'2013-01-01',end)

        # execute the SQL query using execute() method.
    cur.execute(select_record)
    # fetch all of the rows from the query
    rows = cur.fetchall ()

    Views = 0
    UniqueViews = 0
    Barcode = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        Views = Views + row[0]
        UniqueViews = UniqueViews + row[1]
        Barcode = Barcode + row[2]

        RedemptionRatio = "{0:.2f}%".format(Barcode/UniqueViews *100)
        AvgUser = "%0.2f" % (Views/UniqueViews)
    return (UniqueViews,Barcode,RedemptionRatio,AvgUser)

#----------------
def benchmark_week(cur,client_id,start,end):

    start_dt = end - timedelta(days=7)
        # Weekly
    select_record ="""SELECT
                         count((t1.consumer_identity)) AS Views,
                         count(distinct(t1.consumer_identity)) As UniqueViews,
                         sum(case when t1.consumer_offer_transaction_state in ("Presented") then 1 else null end) AS Barcode
                         FROM
							km_analytics.consumer_offer_transaction t1
                          where t1.client_id = %s
                          and t1.consumer_offer_transaction_date between '%s' and '%s'
                        group by t1.promo_id """ % (client_id,start_dt,end)

        # execute the SQL query using execute() method.
    cur.execute(select_record)
    # fetch all of the rows from the query
    rows = cur.fetchall ()

    Views = 0
    UniqueViews = 0
    Barcode = 0

    # Adding content of all rows and returning aggregated values
    for row in rows:
        Views = Views + row[0]
        UniqueViews = UniqueViews + row[1]
        Barcode = Barcode + row[2]

        RedemptionRatio = "{0:.2f}%".format(Barcode/UniqueViews *100)
        AvgUser = "%0.2f" % (Views/UniqueViews)
        Views = "%0.2f" % (Views/7)
        UniqueViews = "%0.2f" % (UniqueViews/7)
        Barcode =  "%0.2f" % (Barcode/7)

    return (UniqueViews,Barcode,RedemptionRatio,AvgUser)


#-------------------------------------------------------------------------------
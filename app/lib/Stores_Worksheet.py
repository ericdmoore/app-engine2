#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Intern
#
# Created:     09/07/2013
# Copyright:   (c) Intern 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import MySQLdb
import sys
from datetime import date, timedelta
import time

def Store_Agg(cur,client_id,start,end):

    select_record = """Select
	consumer_offer_transaction_venue
	, count(*)
	from km_analytics.consumer_offer_transaction
	where client_id = %s
	and consumer_offer_transaction_state in ('Presented')
	group by consumer_offer_transaction_venue
    order by
    count(*) Desc;""" % (client_id)

     # execute the SQL query using execute() method.
    cur.execute(select_record)

    # fetch all of the rows from the query
    query_1 = cur.fetchall ()
    return query_1

def Store_Week(cur,client_id,start,end):

    start_dt = end - timedelta(days=7)
    select_record = """Select
	consumer_offer_transaction_venue
	, count(*)
	from km_analytics.consumer_offer_transaction
	where client_id = %s
	and consumer_offer_transaction_state in ('Presented')
	and consumer_offer_transaction_date between '%s' and '%s'
	group by consumer_offer_transaction_venue order by count(*) Desc;""" % (client_id,start_dt,end)

     # execute the SQL query using execute() method.
    cur.execute(select_record)

    # fetch all of the rows from the query
    query_1 = cur.fetchall ()
    return query_1


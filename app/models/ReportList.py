#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Intern
#
# Created:     25/06/2013
# Copyright:   (c) Intern 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from google.appengine.ext import ndb

class ReportList(ndb.Model):
    ClientId = ndb.StringProperty()
    reportName = ndb.StringProperty()
    clientName = ndb.StringProperty()
    date_start = ndb.DateTimeProperty()
    date_end = ndb.DateTimeProperty()
    frequency = ndb.StringProperty(indexed=False)
    Scheduled = ndb.StringProperty(indexed=False)
    date_created = ndb.DateTimeProperty(auto_now=True)
    offer = ndb.BooleanProperty()
    user  = ndb.BooleanProperty()
    store = ndb.BooleanProperty()
    michaels = ndb.BooleanProperty()
    weekly = ndb.BooleanProperty()
    monthly = ndb.BooleanProperty()
    Scheduled = ndb.StringProperty(indexed=False)